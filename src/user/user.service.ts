import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/user.dto';
import { UserEntity } from './entity/user.entity';

@Injectable()
export class UserService {
  private readonly users: Partial<UserEntity>[] = [];

  create(user: CreateUserDto) {
    this.users.push(user);
    return user as Partial<UserEntity>;
  }

  findAll(): Partial<UserEntity>[] {
    return this.users;
  }
}
