export enum Gender {
  MALE = 'MALE',
  FEMALE = 'FEMALE',
}

export class UserEntity {
  email: string;
  password: string;
  name: string;
  age?: number;
  gender?: Gender;
}
