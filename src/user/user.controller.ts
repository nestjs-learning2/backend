import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UserEntity } from './entity/user.entity';
import { CreateUserDto } from './dto/user.dto';

@Controller('users')
export class UserController {
  constructor(private usersService: UserService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  async findAll(): Promise<Partial<UserEntity>[]> {
    return this.usersService.findAll();
  }

  @Post()
  async create(
    @Body() createUserDto: CreateUserDto,
  ): Promise<Partial<UserEntity>> {
    const user = this.usersService.create(createUserDto);
    return user;
  }
}
